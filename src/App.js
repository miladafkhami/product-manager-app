import React, { Component } from "react";
import Main from "./containers/Main";
import { Provider } from "react-redux";
import configureStore from "./store/store";
import { PersistGate } from "redux-persist/lib/integration/react";
import "./App.scss";
let { store, persistor } = configureStore();

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <div className="App">
            <Main />
          </div>
        </PersistGate>
      </Provider>
    );
  }
}

export default App;
