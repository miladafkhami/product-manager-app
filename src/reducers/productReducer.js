import {
  CREATE_PRODUCT,
  ADD_VARIANT,
  DELETE_PRODUCT,
  DELETE_VARIANT,
  SHOW_ADD_PRODUCT_MODAL,
  SHOW_ADD_VARIANT_MODAL,
  SET_NAVBAR_BTNS,
} from "../actions/index";

const INITIAL_STATE = {
  product: [
    {
      id: 1,
      name: "pride",
      company: "saipa",
      variants: [
        { id: 1, productName: "pride", color: "red", price_MT: 40, entity: 10 },
      ],
      // in larger applications we should first define a new field next to the
      // product field for variants and then link it with it's product by product_id
    },
  ],
  showAddProductModal: false,
  showAddVariantModal: false,
  showNavbarBtns: true,
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case CREATE_PRODUCT: {
      return {
        ...state,
        product: state.product.concat(action.payload),
      };
    }
    case DELETE_PRODUCT: {
      return {
        ...state,
        product: state.product.filter((item) => action.payload !== item.id),
      };
    }
    case ADD_VARIANT: {
      let mod_prods = state.product;
      state.product.map((prod, index) => {
        if (
          prod.name.toLowerCase() === action.payload.productName.toLowerCase()
        )
          mod_prods[index].variants.push(action.payload);
      });
      return {
        ...state,
        product: mod_prods,
      };
    }
    case DELETE_VARIANT: {
      let mod_prods = state.product;
      state.product.map((prod, index) => {
        if (
          prod.name.toLowerCase() === action.payload.productName.toLowerCase()
        )
          mod_prods[index].variants = mod_prods[index].variants.filter(
            (variant) => variant.id !== action.payload.id
          );
      });
      return {
        ...state,
        product: mod_prods,
      };
    }
    case SHOW_ADD_PRODUCT_MODAL: {
      return {
        ...state,
        showAddProductModal: action.payload,
      };
    }
    case SHOW_ADD_VARIANT_MODAL: {
      return {
        ...state,
        showAddVariantModal: action.payload,
      };
    }
    case SET_NAVBAR_BTNS: {
      return {
        ...state,
        showNavbarBtns: action.payload,
      };
    }
    default:
      return state;
  }
};
