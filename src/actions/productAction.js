import {
  CREATE_PRODUCT,
  ADD_VARIANT,
  DELETE_PRODUCT,
  DELETE_VARIANT,
  SHOW_ADD_PRODUCT_MODAL,
  SHOW_ADD_VARIANT_MODAL,
  SET_NAVBAR_BTNS,
} from "./index";

export const createProduct = (request) => {
  return {
    type: CREATE_PRODUCT,
    payload: request,
  };
};

export const deleteProduct = (request) => {
  return {
    type: DELETE_PRODUCT,
    payload: request,
  };
};

export const addVariant = (request) => {
  return {
    type: ADD_VARIANT,
    payload: request,
  };
};

export const deleteVariant = (request) => {
  return {
    type: DELETE_VARIANT,
    payload: request,
  };
};

export const setShowAddProductModal = (request) => {
  return {
    type: SHOW_ADD_PRODUCT_MODAL,
    payload: request,
  };
};

export const setShowAddVariantModal = (request) => {
  return {
    type: SHOW_ADD_VARIANT_MODAL,
    payload: request,
  };
};

export const setShowNavbarBtns = (boolean) => {
  return {
    type: SET_NAVBAR_BTNS,
    payload: boolean,
  };
};
