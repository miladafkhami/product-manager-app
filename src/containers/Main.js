import React from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import Products from "./Products";
import Variants from "./Variants";
import Statistics from "./Statistics";
import AddProduct from "../components/AddProduct";
import AddVariant from "../components/AddVariant";
import { AppBar, Toolbar, Typography, IconButton } from "@material-ui/core";
import EqualizerIcon from "@material-ui/icons/Equalizer";
import { makeStyles } from "@material-ui/core/styles";

import { connect } from "react-redux";

const useStyles = makeStyles(() => ({
  topBarContainer: {
    display: "flex",
    justifyContent: "space-between",
    padding: "0 10%",

    "& svg": {
      height: "30px",
      width: "30px",
    },
  },
}));

const Main = (props) => {
  const classes = useStyles();

  return (
    <Router>
      <AppBar position="static">
        <Toolbar variant="regular" className={classes.topBarContainer}>
          <Typography edge="start" variant="h3" color="inherit">
            Product Manager
          </Typography>
          {props.showNavbarBtns ? (
            <Link to="/statistics" className="bg-primary">
              <IconButton
                edge="end"
                aria-controls="menu-appbar"
                aria-haspopup="true"
                color="inherit"
              >
                <EqualizerIcon />
              </IconButton>
            </Link>
          ) : null}
        </Toolbar>
      </AppBar>
      <Switch>
        <Route exact path="/" component={Products} />
        <Route exact path="/variants/:name" component={Variants} />
        <Route exact path="/statistics" component={Statistics} />
      </Switch>
      {props.showAddProductModal && <AddProduct />}
      {props.showAddVariantModal && <AddVariant />}
    </Router>
  );
};

const mapStateToProps = (state) => ({
  showNavbarBtns: state.product.showNavbarBtns,
  showAddProductModal: state.product.showAddProductModal,
  showAddVariantModal: state.product.showAddVariantModal,
});

const mapActionToProps = {};

export default connect(mapStateToProps, mapActionToProps)(Main);
