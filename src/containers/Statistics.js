import React, { useEffect, createRef, useState } from "react";
import { connect } from "react-redux";
import { Bar } from "react-chartjs-2";
import { setShowNavbarBtns } from "../actions/productAction";

const Statistics = (props) => {
  const [labels, setLabels] = useState([]);
  const [prices, setPrices] = useState([]);
  const [entities, setEntites] = useState([]);

  useEffect(() => {
    const _labels = [],
      _prices = [],
      _entities = [];
    props.product.map((product) => {
      product.variants.map((variant) => {
        _labels.push(`${product.name} ${variant.color}`);
        setLabels(_labels);
        _prices.push(variant.price_MT);
        setPrices(_prices);
        _entities.push(variant.entity);
        setEntites(_entities);
      });
    });

    props.setShowNavbarBtns(false);
    return () => {
      props.setShowNavbarBtns(true);
    };
  }, []);

  const data = (type) => {
    return {
      labels,
      datasets: [
        {
          label: `${type} / Variant`,
          backgroundColor: "rgba(255,99,132,0.4)",
          borderColor: "#f50057",
          borderWidth: 1,
          hoverBackgroundColor: "#f50057",
          hoverBorderColor: "rgba(255,99,132,1)",
          data: type === "Entity" ? entities : prices,
        },
      ],
    };
  };

  return (
    <div>
      <div>
        <h2>Entity / Variant</h2>
        <Bar
          data={data("Entity")}
          width={100}
          options={{
            maintainAspectRatio: false,
          }}
        />
      </div>
      <div>
        <h2>Price / Variant</h2>
        <Bar
          data={data("Price")}
          width={100}
          options={{
            maintainAspectRatio: false,
          }}
        />
      </div>
    </div>
  );
};

const mapStateToProps = (state) => ({
  product: state.product.product,
});

const mapActionToProps = {
  setShowNavbarBtns,
};

export default connect(mapStateToProps, mapActionToProps)(Statistics);
