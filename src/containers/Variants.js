import React, { useEffect, useState } from "react";

import VariantList from "../components/VariantList";
import { Grid, Button } from "@material-ui/core";

import { connect } from "react-redux";
// import { deleteVariant } from "../actions/productAction";
import {
  createVariant,
  deleteVariant,
  setShowAddVariantModal,
} from "../actions/productAction";
const Variants = (props) => {
  const [variants, setVariants] = useState([]);

  useEffect(() => {
    let _var = [];
    props.product.map((prod) => {
      if (prod.name.toLowerCase() === props.match.params.name.toLowerCase())
        _var = prod.variants;
    });
    setVariants(_var);
  }, [props.product]);

  return (
    <Grid container className="d-flex justify-content-around">
      <Grid md={9} xs={12} className="p-1">
        <VariantList list={variants} />
        <Button
          variant="contained"
          size="large"
          color="primary"
          className="font-smaller w-100"
          onClick={() => {
            props.setShowAddVariantModal(true);
          }}
        >
          Add Variant
        </Button>
      </Grid>
    </Grid>
  );
};

const mapStateToProps = (state) => ({
  product: state.product.product,
});

const mapActionToProps = {
  // deleteProduct,
  setShowAddVariantModal,
};

export default connect(mapStateToProps, mapActionToProps)(Variants);
