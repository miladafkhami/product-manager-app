import React, { Component } from "react";
import ProductList from "../components/ProductList";
import { Grid, Button } from "@material-ui/core";
import { connect } from "react-redux";
import {
  createProduct,
  deleteProduct,
  setShowAddProductModal,
} from "../actions/productAction";

class Products extends Component {
  constructor() {
    super();
  }

  render() {
    return (
      <Grid container className="d-flex justify-content-around">
        <Grid md={9} xs={12} className="p-1">
          <ProductList list={this.props.product} title="Products" />
          <Button
            variant="contained"
            size="large"
            color="primary"
            className="w-100 font-smaller"
            onClick={() => {
              this.props.setShowAddProductModal(
                true
                // !this.props.showAddProductModal
              );
            }}
          >
            Add Product
          </Button>
        </Grid>
      </Grid>
    );
  }
}

const mapStateToProps = (state) => ({
  product: state.product.product,
  showAddProductModal: state.product.showAddProductModal,
});

const mapActionToProps = {
  createProduct,
  deleteProduct,
  setShowAddProductModal,
};

export default connect(mapStateToProps, mapActionToProps)(Products);
