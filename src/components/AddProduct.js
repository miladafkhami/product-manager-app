import React, { useEffect, useState } from "react";
import {
  Modal,
  TextField,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  Button,
  FormControlLabel,
  Switch,
} from "@material-ui/core";
import { connect } from "react-redux";
import {
  createProduct,
  setShowAddProductModal,
} from "../actions/productAction";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-around",
    alignItems: "center",
    width: "50%",
    maxWidth: "400px",
    minWidth: "300px",
    height: "auto",
    maxHeight: "65%",
    overflowY: "overlay",
    margin: "10% auto 0",
    background: "white",
    borderRadius: "5px",
    outline: "none",
    padding: "0 10px",
    "& > *": {
      margin: theme.spacing(1),
      width: "100%",
    },
    "& *": {
      fontSize: "16px",
    },
  },
  modalHeading: {
    borderBottom: "1px solid gray",
    textAlign: "center",
    color: "gray",
    paddingBottom: "7px",
  },
}));

const AddProduct = (props) => {
  const classes = useStyles();

  // const id = new Date().valueOf();
  const [id, setId] = useState(new Date().valueOf());
  const [name, setName] = useState("");
  const [company, setCompany] = useState("");

  // useEffect(() => {
  //   return () => {
  //     props.setOnOperationTask(null);
  //   };
  // }, []);

  const saveProduct = (e) => {
    e.preventDefault();
    let newProd = {
      id,
      name,
      company,
      variants: [],
    };
    // props.task?.id ? props.updateTask(taskObj) :
    props.createProduct(newProd);
    handleClose();
    setName("");
    setCompany("");
  };

  const handleClose = () => {
    props.setShowAddProductModal(false);
  };

  return (
    <Modal
      open={props.showAddProductModal}
      onClose={handleClose}
      className="d-flex"
    >
      <form className={classes.root} onSubmit={saveProduct}>
        <TextField
          label="name"
          id="product-name"
          placeholder="Name"
          type="text"
          value={name}
          onChange={(e) => setName(e.target.value)}
          autoComplete="off"
          autoFocus
        />
        <TextField
          label="Company"
          id="product-company"
          placeholder="Company"
          type="text"
          value={company}
          onChange={(e) => setCompany(e.target.value)}
          autoComplete="off"
        />

        <Button type="submit" variant="contained" color="primary">
          Add Product
        </Button>
      </form>
    </Modal>
  );
};

const mapStateToProps = (state) => ({
  showAddProductModal: state.product.showAddProductModal,
});

const mapActionToProps = {
  createProduct,
  setShowAddProductModal,
};

export default connect(mapStateToProps, mapActionToProps)(AddProduct);
