import React, { useEffect, useState } from "react";
import { Modal, TextField, Button } from "@material-ui/core";
import { connect } from "react-redux";
import { addVariant, setShowAddVariantModal } from "../actions/productAction";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-around",
    alignItems: "center",
    width: "50%",
    maxWidth: "400px",
    minWidth: "300px",
    height: "auto",
    maxHeight: "65%",
    overflowY: "overlay",
    margin: "10% auto 0",
    background: "white",
    borderRadius: "5px",
    outline: "none",
    padding: "0 10px",
    "& > *": {
      margin: theme.spacing(1),
      width: "100%",
    },
    "& *": {
      fontSize: "16px",
    },
  },
  modalHeading: {
    borderBottom: "1px solid gray",
    textAlign: "center",
    color: "gray",
    paddingBottom: "7px",
  },
}));

const AddVariant = (props) => {
  const classes = useStyles();

  const id = new Date().valueOf();
  const [productName, setProductName] = useState(
    window.location.pathname.split("/").pop()
  );
  const [color, setColor] = useState("");
  const [price_MT, setPrice_MT] = useState();
  const [entity, setEntity] = useState();

  const saveProduct = (e) => {
    e.preventDefault();
    let newVariant = {
      id,
      productName,
      color,
      price_MT,
      entity,
    };
    props.addVariant(newVariant);

    handleClose();
    setProductName("");
    setColor("");
    setPrice_MT();
    setEntity();
  };

  const handleClose = () => {
    props.setShowAddVariantModal(false);
  };

  return (
    <Modal
      open={props.showAddVariantModal}
      onClose={handleClose}
      className="d-flex"
    >
      <form className={classes.root} onSubmit={saveProduct}>
        <TextField
          label="Product Name"
          id="product-name"
          placeholder="Product Name"
          type="text"
          value={productName}
        />
        <TextField
          label="Color"
          id="variant-color"
          placeholder="Color"
          type="text"
          value={color}
          onChange={(e) => setColor(e.target.value)}
          autoComplete="off"
          autoFocus
        />
        <TextField
          label="Price"
          id="variant-price"
          placeholder="Price (MT)"
          type="number"
          value={price_MT}
          onChange={(e) => setPrice_MT(e.target.value)}
          autoComplete="off"
        />
        <TextField
          label="Entity"
          id="variant-entiry"
          placeholder="Entity (MT)"
          type="number"
          value={entity}
          onChange={(e) => setEntity(e.target.value)}
          autoComplete="off"
        />
        <Button
          type="submit"
          variant="contained"
          color="primary"
          className="w-100 font-smaller"
        >
          Add Variant
        </Button>
      </form>
    </Modal>
  );
};

const mapStateToProps = (state) => ({
  showAddVariantModal: state.product.showAddVariantModal,
});

const mapActionToProps = {
  addVariant,
  setShowAddVariantModal,
};

export default connect(mapStateToProps, mapActionToProps)(AddVariant);
