import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";

import { Grid } from "@material-ui/core";

import { connect } from "react-redux";
import { deleteProduct } from "../actions/productAction";

class ProductListItem extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { id, name, company, variants } = this.props.product;

    return (
      <Grid
        container
        className="my-1 font-bigger"
        onClick={() => {
          this.props.history.push({
            pathname: `/variants/${name}`,
            state: { id, name },
          });
        }}
      >
        <Grid xs={6} className="d-flex">
          <span className="w-100 h-100 text-capitalize">{name}</span>
        </Grid>
        <Grid xs={3}>
          <span className="d-flex justify-content-start align-items-center text-light text-capitalize">
            {company}
          </span>
        </Grid>
        <Grid
          xs={2}
          className="d-flex justify-content-start align-items-center"
        >
          {variants?.length || 0}
        </Grid>
        <Grid
          xs={1}
          className="d-flex justify-content-start align-items-center"
          onClick={(e) => {
            e.stopPropagation();
            this.props.deleteProduct(id);
          }}
        >
          <i className="glyphicon glyphicon-trash cursor-pointer"></i>
        </Grid>
      </Grid>
    );
  }
}

const mapStateToProps = (state) => ({});

const mapActionToProps = {
  deleteProduct,
};

export default connect(
  mapStateToProps,
  mapActionToProps
)(withRouter(ProductListItem));
