import React from "react";
import VariantListItem from "./VariantListItem";
import { Grid } from "@material-ui/core";

const VariantList = (props) => {
  return (
    <div className="panel panel-default">
      <div className="panel-heading bg-primary">
        <Grid container className="my-1 font-bigger">
          <Grid xs={6}>Color</Grid>
          <Grid xs={3}>
            Price <small className="text-info">(MT)</small>
          </Grid>
          <Grid xs={2}>Entity</Grid>
          <Grid xs={1}></Grid>
        </Grid>
      </div>
      <div className="panel-body p-0">
        {props.list && props.list.length > 0 ? (
          props.list.map((variant, index) => {
            return (
              <Grid
                sm={12}
                key={index}
                className="list-group-item b-none hover-shadow"
              >
                <VariantListItem variant={variant} />
              </Grid>
            );
          })
        ) : (
          <div className="font-normal text-info m-1 text-center">
            There is no variant here! Try Adding One.
          </div>
        )}
      </div>
    </div>
  );
};

export default VariantList;
