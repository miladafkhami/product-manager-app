import React from "react";
import ProductListItem from "./ProductListItem";
import { Grid } from "@material-ui/core";

const ProductList = (props) => {
  return (
    <div className={"panel panel-default"}>
      <div className="panel-heading bg-primary text-uppercase">
        {props.title}
      </div>
      <div className="panel-body p-0">
        {props.list.length > 0 ? (
          props.list.map((product, index) => {
            return (
              <Grid
                sm={12}
                key={index}
                className="list-group-item b-none hover-shadow cursor-pointer"
              >
                <ProductListItem product={product} />
              </Grid>
            );
          })
        ) : (
          <div className="font-normal text-secondary m-1 text-center">
            There is no product here! Try Adding One.
          </div>
        )}
      </div>
    </div>
  );
};

export default ProductList;
