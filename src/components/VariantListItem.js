import React from "react";

import { Grid } from "@material-ui/core";

import { connect } from "react-redux";
import { deleteVariant } from "../actions/productAction";

const ProductListItem = (props) => {
  const { id, color, price_MT, entity, productName } = props.variant;

  return (
    <>
      <Grid container className="my-1 text-capitalize font-bigger">
        <Grid xs={6} className="text-capitalize">
          {color}
        </Grid>
        <Grid xs={3} className="text-light">
          {price_MT}
        </Grid>
        <Grid xs={2}>{entity}</Grid>
        <Grid
          xs={1}
          onClick={() => {
            props.deleteVariant({ id, productName });
            window.location.reload();
          }}
        >
          <i className="glyphicon glyphicon-trash cursor-pointer"></i>
        </Grid>
      </Grid>
    </>
  );
};

const mapStateToProps = (state) => ({});

const mapActionToProps = {
  deleteVariant,
};

export default connect(mapStateToProps, mapActionToProps)(ProductListItem);
